from random import choice
from django.http.response import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.urls import reverse, reverse_lazy
from django.views import generic
from .models import *
from .forms import *

# Create your views here.
def index(req):
    vegetariano = list(Category.objects.get(name='Vegetarianos').recipe.exclude(description__isnull=True).exclude(description__exact=''))
    carne = list(Category.objects.get(name='Carnes').recipe.exclude(description__isnull=True).exclude(description__exact=''))
    sobremesa = list(Category.objects.get(name='Sobremesa').recipe.exclude(description__isnull=True).exclude(description__exact=''))

    pratos = {
        'vegetariano': choice(vegetariano),
        'sobremesa': choice(sobremesa)
        }

    vegetariano.remove(pratos['vegetariano'])
    dia = vegetariano + carne

    pratos.update({'dia': choice(dia)})

    return render(req, 'recipes/index.html', context={'prato': pratos})

def about(req):
    return render(req, 'recipes/about.html', context={})

class RecipeList(generic.ListView):
    model = Post
    template_name = 'recipes/list.html'
    context_object_name = 'recipes'

class RecipeDetails(generic.DetailView):
    model = Post
    template_name = 'recipes/recipe.html'
    context_object_name = 'recipe'

class RecipeCreate(generic.CreateView):
    model = Post
    form_class = RecipeForm
    template_name = 'recipes/create.html'

    def get_success_url(self):
        return reverse('recipe_detail', args=(self.object.id,))
    
class RecipeUpdate(generic.UpdateView):
    model = Post
    form_class = RecipeForm
    template_name = 'recipes/create.html'
    
    def get_initial(self):
        recipe = self.object
        initial = {
            'title': recipe.title,
            'picture': recipe.picture,
            'content': recipe.content,
            'description': recipe.description,
        }
        return initial

    def get_success_url(self):
        return reverse('recipe_detail', args=(self.object.id,))

class RecipeDeleteConfirm(generic.DeleteView):
    model = Post
    template_name = 'recipes/delete.html'
    success_url = reverse_lazy('recipes')
    context_object_name = 'recipe'

def CommentCreate(req, pk):
    recipe = get_object_or_404(Post, id=pk)
    if req.method == 'POST':
        form = CommentForm(req.POST)
        if form.is_valid():
            author = form.cleaned_data['author']
            text = form.cleaned_data['text']
            comment = Comment(author=author, text=text, recipe=recipe)
            comment.save()
            return HttpResponseRedirect(reverse('recipe_detail', args=(pk, )))
    else:
        form = CommentForm()
    return render(req, 'recipes/comment.html', context={'form': form, 'recipe': recipe})

def CategoryDetail(req, pk):
    category = get_object_or_404(Category, id=pk) 
    return render(req, 'recipes/list.html', context={'recipes': category.recipe.all(), 'title': category.name, 'description': category.description})

def CategoryList(req):
    return render(req, 'recipes/categories.html', context={'categories': Category.objects.all()})