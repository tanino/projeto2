from django.db.models import fields
from django.db.models.base import Model
from django.forms import ModelForm, widgets, CharField
from .models import *

class RecipeForm(ModelForm):
    class Meta:
        model = Post
        fields = [
            'title',
            'picture',
            'ingredients',
            'making',
            'description',
        ]
        labels = {
            'title': 'Título da postagem',
            'picture': 'URL da imagem',
            'ingredients': 'Ingredientes da receita',
            'making': 'Modo de preparo',
            'description': 'Descrição da receita',
        }
        widgets = {
            'ingredients': widgets.Textarea(attrs={'cols': 50, 'rows': 10}),
            'making': widgets.Textarea(attrs={'cols': 50, 'rows': 10}),
            'description': widgets.Textarea(attrs={'cols': 50, 'rows': 10}),
        }

class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = ['author', 'text']
        labels = {
            'author': 'Autor',
            'text': 'Comentário',
        }
        widgets = {
            'text': widgets.Textarea(attrs={'cols': 50, 'rows': 10}),
        }