from django.db import models
from django.conf import settings

# Create your models here.
class Post(models.Model):
    title = models.CharField(max_length=255)
    picture = models.URLField(max_length=200, null=True, blank=True)
    ingredients = models.TextField(max_length=5000)
    making = models.TextField(max_length=5000)
    content = models.TextField(max_length=5000)
    description = models.TextField(max_length=255, null=True, blank=True)
    post_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.title}: Postado em {self.post_date}'

    def save(self, *args, **kwargs):
        self.content = '<h2>Ingredientes</h2>\n<ul>\n'
        for ing in self.ingredients.splitlines():
            if ing == '':
                pass
            elif ing[0] == '-':
                self.content += f'    <li>{ing[1:]}\n'
            else:
                if self.content[-5:] == '<ul>\n':
                    self.content = self.content[:-5]
                self.content += f'<h3>{ing}</h3>\n'
        self.content += '</ul>\n<br>\n<h2>Modo de preparo</h2>\n<p>\n'
        for line in self.making.splitlines():
            self.content += f'{line}\n<br>'
        self.content += '\n</p>'
        super().save(*args, **kwargs)

class Comment(models.Model):
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    text = models.TextField(max_length=5000)
    post_date = models.DateTimeField(auto_now_add=True)
    recipe = models.ForeignKey(Post, on_delete=models.CASCADE)

Comment.objects.order_by('post_date')

class Category(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField(max_length=100)
    recipe = models.ManyToManyField(Post)