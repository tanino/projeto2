from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('about', views.about, name='about'),
    path('create', views.RecipeCreate.as_view(), name='create'),
    path('recipes', views.RecipeList.as_view(), name='recipes'),
    path('recipes/<int:pk>', views.RecipeDetails.as_view(), name='recipe_detail'),
    path('delete/<int:pk>', views.RecipeDeleteConfirm.as_view(), name='delete'),
    path('update/<int:pk>', views.RecipeUpdate.as_view(), name='update'),
    path('comment/create/<int:pk>', views.CommentCreate, name='create_comment'),
    path('categories', views.CategoryList, name='categories'),
    path('categories/<int:pk>', views.CategoryDetail, name='category_detail'),
]